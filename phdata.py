#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Module for managing persistent hierarchical data.

@author: Simon Thiesen
"""

from sys import version_info
from os import PathLike
from pathlib import Path
from pickle import dumps, loads
from sqlite3 import Connection, connect
from weakref import ref, ReferenceType
from collections import deque
from typing import List, Dict, Any, Union

class _PHDNode(object):
    """
    Base node class
    """

    def __new__(cls, root: Any, node_id: int) -> Any:
        self = super().__new__(cls)
        self._root = root
        self._node_id = node_id
        if cls == _PHDNode:
            self._root._alive.append(self)
            return ref(self)
        return self

    def __del__(self) -> None:
        while self in self._root._alive:
            self._root._alive.remove(self)

    def __getattribute__(self, name: str) -> Any:
        if not name.startswith('_'):
            self._root._alive.append(self)
            while self._root._alive.count(self) > 1:
                self._root._alive.remove(self)
        try:
            get_attr = super().__getattribute__(name)
            if name == 'n_' and get_attr == None:
                content = self.__get_node_content()
                if content != None:
                    self.__setattr__(name, content)
                return content
            if type(get_attr) == ReferenceType:
                if get_attr() != None:
                    return get_attr()
                else:
                    raise AttributeError()
            return get_attr
        except (AttributeError):
            if name == 'n_':
                self.__setattr__(name, self.__get_node_content())
            else:
                self.__setattr__(name, None)
            return self.__getattribute__(name)

    def __setattr__(self, name: str, value: Any) -> None:
        if len(name)>255:
            raise ValueError("Name is too long for database storage")
        elif name.startswith('_'):
            super().__setattr__(name, value)
        elif name == 'n_':
            if value != self.__get_node_content():
                self.__set_node_content(value)
            super().__setattr__(name, value)
        elif name in self.__dict__.keys() and value != None:
            raise ValueError("Nodes cannot be modified direktly. Use root functions instead.")
        else:
            new_node = self.__get_child_node(name)
            super().__setattr__(name, new_node)

    def __set_node_content(self, value: Any) -> None:
        value_pickled = dumps(value)
        SQL_UPDATE_NODE_CONTENT = "UPDATE phd_table SET content = ? WHERE node_id = ?"
        if value == None:
            self._root._con.execute(SQL_UPDATE_NODE_CONTENT, (None, self._node_id))
        else:
            self._root._con.execute(SQL_UPDATE_NODE_CONTENT, (value_pickled, self._node_id))

    def __get_node_content(self) -> Any:
        SQL_SELECT_NODE_CONTENT = "SELECT content FROM phd_table WHERE node_id = ?"
        for result in self._root._con.execute(SQL_SELECT_NODE_CONTENT, (self._node_id, )):
            if result[0] == None:
                return None
            return loads(result[0])

    def __create_node_child(self, name: str) -> None:
        SQL_INSERT_CHILD_NODE = "INSERT INTO phd_table (name, parent_id) VALUES (?, ?)"
        self._root._con.execute(SQL_INSERT_CHILD_NODE, (name, self._node_id))

    def __get_child_node(self, name: str) -> Any:
        SQL_SELECT_CHILD_NODE = "SELECT node_id FROM phd_table WHERE parent_id = ? AND name = ?"
        for result in self._root._con.execute(SQL_SELECT_CHILD_NODE, (self._node_id, name)):
            return _PHDNode(self._root, result[0])
        self.__create_node_child(name)
        return self.__get_child_node(name)

    def _child_nodes(self) -> List:
        """
        List names of all sub-nodes.
        """
        SQL_SELECT_CHILD_NODES = "SELECT name FROM phd_table WHERE parent_id = ?"
        return [row[0] for row in self._root._con.execute(SQL_SELECT_CHILD_NODES, (self._node_id, ))]

    def _delete_persistent(self) -> None:
        """
        Remove the node and all sub-nodes from database.
        """
        SQL_DELETE_CHILD_NODE = "DELETE FROM phd_table WHERE node_id = ?"
        SQL_DELETE_SUBCHILDS = """DELETE FROM phd_table WHERE parent_id NOT IN
                                  (SELECT node_id FROM phd_table)"""
        rc: int = self._root._con.execute(SQL_DELETE_CHILD_NODE, (self._node_id, )).rowcount
        while rc > 0:
            rc = self._root._con.execute(SQL_DELETE_SUBCHILDS).rowcount
        self.__del__()

    def _move_branch(self, new_node: Any) -> None:
        """
        Move the called node to another higher-level node.
        """
        if new_node._node_id == self._node_id:
            raise ValueError("Cannot move to identical node!")
        if id(self) == id(self._root):
            raise ValueError("Cannot move the root node!")
        SQL_UPDATE_NODE_PARENT = "UPDATE phd_table SET parent_id = ? WHERE node_id = ?"
        self._root._con.execute(SQL_UPDATE_NODE_PARENT, (new_node._node_id, self._node_id))
        self.__del__()

    def _set_mutable_cont(self) -> None:
        self.__set_node_content(self.n_)


class PHDRoot(_PHDNode):
    """
    Root class connect to database and present struct like access to a huge among of hierachical structured data.

    Parameters
    ----------
    database : {str, path-like}
        path to sqlite database
    weak_ref : int
        max in RAM node objects
    """

    _databases: Dict[Path, _PHDNode] = {}

    def __new__(cls, database: Union[PathLike, str], weak_ref: int = 200) -> Any:
        if weak_ref < 20:
            raise ValueError("{wr} is too small for convenient usage (>=20).".format(wr=weak_ref))
        if version_info[0]>3 and version_info[1]>5:
            db_path = Path(database).resolve(strict=True)
        else:
            db_path = Path(database).resolve()
        if db_path in PHDRoot._databases.keys():
            return PHDRoot._databases[db_path]
        else:
            SQL_CREATE_TABLE = """CREATE TABLE IF NOT EXISTS phd_table
                                  (node_id INTEGER PRIMARY KEY, name VARCHAR(255),
                                  content BLOB, parent_id INTEGER);
                                  CREATE UNIQUE INDEX IF NOT EXISTS idx_node_id ON phd_table (node_id);
                                  CREATE INDEX IF NOT EXISTS idx_parent_id ON phd_table (parent_id);
                                  CREATE UNIQUE INDEX IF NOT EXISTS idx_name_parent ON phd_table (name, parent_id);"""
            SQL_SELECT_ROOT_NODE = "SELECT node_id FROM phd_table WHERE parent_id IS NULL LIMIT 1"
            sql_con = connect(db_path, isolation_level = None)
            sql_con.executescript(SQL_CREATE_TABLE)
            if sql_con.execute(SQL_SELECT_ROOT_NODE).rowcount<1:
                SQL_INSERT_ROOT_NODE = "INSERT INTO phd_table (name) VALUES (?)"
                sql_con.execute(SQL_INSERT_ROOT_NODE, ('root', ))
            for result in sql_con.execute(SQL_SELECT_ROOT_NODE):
                new_inst = super().__new__(cls, None, result[0])
                new_inst._db_path = db_path
                new_inst._con = sql_con
                new_inst._alive = deque([], weak_ref)
                new_inst._root = new_inst
                PHDRoot._databases[db_path] = new_inst
                return new_inst

    def __del__(self) -> None:
        self._con.close()
        del PHDRoot._databases[self._db_path]
