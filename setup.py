from setuptools import setup
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name="phdata",
    py_modules=["phdata"],
    version="1.0.0",
    description="Persistent hierachical data storage",
    long_description = long_description,
    author="Simon Thiesen",
    url="https://bitbucket.org/sithies/phdata/",
    download_url="https://bitbucket.org/sithies/phdata/",
    keywords=["hierachical", "database", "struct"],
    classifiers=[
        "Programming Language :: Python :: 3.4",
        "Development Status :: 3 - Alpha",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Intended Audience :: Science/Research",
        "Topic :: Scientific/Engineering",
        "Topic :: Database :: Front-Ends",
        ],
    # include_package_data=True
)
