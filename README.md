phdata
======

A class for storing and managing hierachical data with inherent persistent mode.

The `PHDRoot(database, weak_ref = 200)` class returns a root node, which is connected to a sqlite database. In that hierachical data is stored and it is accessible via dot-notation, like a struct array. Compared to that it is allow to save data in every node.

## How to be used

At first a new root object has to be created. Therefore the class get at least one parameter for the path of the database file (pathlike-objects are also allowed). The optional parameter `weak_ref` expect a integer at least 20; standard is 200 (functionality is described later):

    test = PHDRoot('test.db', weak_ref = 50)

With this `test` object it can be worked like a MATLAB struct:

    test.test1.test2.n_ = 42
    test.test_it.n_ = "Try it!"
    test.test1.test_47.n_ = (34, "Hello World!")

In the background the root object creates a tree-like sequenz of new sub-nodes. Every node has its own content storage, accessed by the build in variable `n_`. All content and the node-structure is saved in the relational database as self-related datasets. In this process the content is saved as bytes pickled by the `pickle` module. So all data, which is pickle-able can be stored. All allowed variable names can be used except ones which starts with an underline and not longer than 255 characters.

Due to the way of saving data to the database there is a problem with mutable objects:

    test.test1.test51.n_ = {"black": "schwarz", "blue": "blau"}
    test.test1.test51.n_["green"] = "grün"

As expected after the first step the database contains the displayed dictionary. But after the second step the database entry hasn't changed: the object can't notice, that the object in variable `n_` is modified. This problem can be solved with following function:

    test.test1.test51._set_mutable_cont()

After applying the database contains the modified object. The function has to be called in order to submit the changes to database.

It is no problem to access multiple databases at the same time, but if the same one is accessed again the already existing root is returned.

## Meaning of weak_ref

This struct object is created for huge among of data. So it is not recommended to keep the whole objects of hierachical tree in RAM. Thus there are two points to manage the data accessibility. First, the just desribed database which allowes a random seek to the data. The second point is more complex. The nodes are stored in a queue, which can only hold ``weak_ref`` entries. If a new node is added the oldest one will be removed. Oldest means that it has not been requested for the longest time. Standard queue length is 200; certainly enough for most application.

## Usefull node-depending functions

In addition to the described function above, there are several other functions:

* `_delete_persistent()`: Only way to delete a node from database. If called all sub-nodes are also deleted from database.
* `_move_branch(new_node)`: This function moves the current node (and all sub-nodes) to `new_node`. Doesn't work from one root node to another and dont check if new node is sub-node of the old one!
* `_child_nodes()`: Returns the names of sub-nodes of the current node.
