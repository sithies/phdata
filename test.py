import unittest
import pickle

from phdata import PHDRoot

class PHDataTest(unittest.TestCase):

    def testCalculation(self) -> None:
        root1 = PHDRoot('test.db')

        root1.some.hierachical.infos.n_ = {1: 1, 2: 4}
        SQL_SELECT_NODE_CONTENT = "SELECT content FROM phd_table WHERE node_id = ?"
        result = root1._con.execute(SQL_SELECT_NODE_CONTENT, (root1.some.hierachical.infos._node_id, ))
        self.assertDictEqual(pickle.loads(result.fetchone()[0]), root1.some.hierachical.infos.n_)
        root1.some.hierachical.infos.n_[3] = 9
        root1.some.hierachical.infos._set_mutable_cont()
        result = root1._con.execute(SQL_SELECT_NODE_CONTENT, (root1.some.hierachical.infos._node_id, ))
        self.assertDictEqual(pickle.loads(result.fetchone()[0]), root1.some.hierachical.infos.n_)

        root1.some.hierachical.about.n_ = 42
        self.assertEqual(42, root1.some.hierachical.about.n_)
        root1.much.more.n_ = "Hello Wörld!"
        self.assertEqual("Hello Wörld!", root1.much.more.n_)
        self.assertCountEqual(root1._child_nodes(), ['some', 'much'])

        root1.some.hierachical._move_branch(root1)
        self.assertCountEqual(root1._child_nodes(), ['some', 'much', 'hierachical'])

        root1.much.more._delete_persistent()
        self.assertEqual(root1.much.more.n_, None)

if __name__ == "__main__":
    unittest.main()
